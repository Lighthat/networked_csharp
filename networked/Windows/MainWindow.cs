﻿using System;
using Gtk;
namespace networked
{
	public partial class MainWindow : Gtk.Window
	{
		Label  _label;
		Button _button;

		public MainWindow (Controller controller) :
			base (Gtk.WindowType.Toplevel)
		{
			DebugUtility.PrintDebugText ("This is some test text on MainWindow construction.");
			this.Resize (200, 200);
			this.DeleteEvent += OnDelete;

			_label  = new  Label ("0");
			_button = new Button (_label);

			_button.Clicked += new EventHandler ( controller.testEvent );

			this.Add (_button);

			this.ShowAll ();
		}

		/// <summary>
		/// Updates the value on the button's label
		/// </summary>
		/// <param name="text">What the label should say</param>
		public void UpdateLabel(string text)
		{
			_label.Text = text;
		}
			
		static void OnDelete(object obj, DeleteEventArgs arguments)
		{
			Application.Quit ();
		}
			
	}



}

