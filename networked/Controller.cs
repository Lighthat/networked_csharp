﻿using System;
using Gtk;
using System.Collections.Generic;

namespace networked
{
	public class Controller
	{
		Model _model;
		View  _view;

		public Controller ()
		{
			_model = new Model ();
			_view = new View (this, _model);
		}

		/// <summary>
		/// Tests handling button click events
		/// </summary>
		/// <param name="obj">The button that was clicked</param>
		/// <param name="arguments">Arguments. Not sure how to use these yet</param>
		public void testEvent(object obj, EventArgs arguments)
		{
			_model.IncrementLabel ();
		}
	}
}

