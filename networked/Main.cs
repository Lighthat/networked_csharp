﻿using System;
using Gtk;
namespace networked
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Application.Init ();
			DebugUtility.Enable ();
			Controller controller = new Controller ();
			Application.Run ();
		}
	}
}