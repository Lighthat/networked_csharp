﻿using System;

namespace networked
{
	public class Model : IObservable<string>
	{
		private IObserver<string> _observer;
		private int  _value;

		public Model ()
		{
			_value = 0;
		}

		/// <summary>
		/// A test function that will increment the value on the label on MainWindow.
		/// </summary>
		public void IncrementLabel()
		{
			++_value;
			_observer.OnNext (_value.ToString ());
		}

		/// <returns>An Unsubscriber that can allow the View to clean up when the program terminates.</returns>
		/// <summary>
		/// Connects the Model to the View
		/// </summary>
		/// <param name="observer">The View</param>
		public IDisposable Subscribe(IObserver<string> observer) 
		{
			_observer = observer;
			return new Unsubscriber(observer);
		}

		/// <summary>
		/// The Unsubscriber class that allows observers to clean up an observable object
		/// </summary>
		private class Unsubscriber : IDisposable
		{
			private IObserver<string> __observer;

			/// <summary>
			/// Initializes a new instance of the <see cref="networked.Model+Unsubscriber"/> class.
			/// </summary>
			/// <param name="observer">The View</param>
			public Unsubscriber(IObserver<string> observer)
			{
				__observer = observer;
			}

			/// <summary>
			/// Releases all resource used by the <see cref="networked.Model+Unsubscriber"/> object.
			/// </summary>
			/// <remarks>Call <see cref="Dispose"/> when you are finished using the <see cref="networked.Model+Unsubscriber"/>. The
			/// <see cref="Dispose"/> method leaves the <see cref="networked.Model+Unsubscriber"/> in an unusable state. After
			/// calling <see cref="Dispose"/>, you must release all references to the <see cref="networked.Model+Unsubscriber"/>
			/// so the garbage collector can reclaim the memory that the <see cref="networked.Model+Unsubscriber"/> was occupying.</remarks>
			public void Dispose()
			{
				DebugUtility.PrintDebugText (__observer.GetHashCode () + " Unsubscribe.");
			}
		}
	}
}

