﻿using System;

namespace networked
{
	public class View : IObserver<string>
	{
		private Controller _controller;
		private IDisposable _unsubscriber;
		private MainWindow _mainWindow;

		public View (Controller controller, Model model)
		{
			_controller = controller;
			_mainWindow = new MainWindow (_controller);
			Subscribe (model);
		}


		/// <summary>
		/// Allows the View to observe the Model
		/// </summary>
		/// <param name="provider">The Model</param>
		public void Subscribe(IObservable<string> provider)
		{
			DebugUtility.PrintDebugText ("Subscribe in view.");

			if (provider != null) 
				_unsubscriber = provider.Subscribe(this);
		}

		/// <summary>
		/// Raises the completed event.
		/// </summary>
		public void OnCompleted()
		{
			DebugUtility.PrintDebugText ("On Completed.");
			this.Unsubscribe();
		}

		/// <summary>
		/// Raises the error event.
		/// </summary>
		/// <param name="e">E.</param>
		public void OnError(Exception e)
		{
			DebugUtility.PrintDebugText (e.Message);
		}

		/// <summary>
		/// This handles an event from the Model. It will tell the View what
		/// window to show next.
		/// </summary>
		/// <param name="window">The window to be shown</param>
		public void OnNext(string window)
		{
			DebugUtility.PrintDebugText (window);
			_mainWindow.UpdateLabel (window);
		}

		/// <summary>
		/// Cleans up the Model on program close.
		/// </summary>
		public void Unsubscribe()
		{
			_unsubscriber.Dispose();
		}
	}
}

